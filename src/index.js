import 'phaser';
import config from './config/config';
import PreloaderScene from './scenes/PreloaderScenes';
import BootScene from './scenes/BootScene';
import MVCGameScene from './scenes/MVCGameScene';

class Game extends Phaser.Game {
  constructor() {
    super(config);
    this.scene.add('Boot', BootScene);
    this.scene.add('Preloader', PreloaderScene);
    this.scene.add('MVCGame', MVCGameScene);
    this.scene.start('MVCGame');
  }

  resize() {
    let canvas = document.querySelector("canvas");
    let windowWidth = window.innerWidth;
    let windowHeight = window.innerHeight;
    if (windowHeight >= windowWidth) {
      canvas.style.width = windowWidth + "px";
      canvas.style.height = windowWidth + "px";
    } else {
      canvas.style.width = windowHeight + "px";
      canvas.style.height = windowHeight + "px";

    }
  }
}

let game = new Game();
window.addEventListener("resize", game.resize, false);