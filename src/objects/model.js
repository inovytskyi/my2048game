export default class Model {
    constructor() {
        this.tiles = [];
        window.tiles = this.tiles;
        this.onNew = null;
        this.onMove = null;
        for (let y = 0; y < 4; y++) {
            this.tiles[y] = [];
            for (let x = 0; x < 4; x++) {
                this.tiles[y][x] = {
                    value: 0,
                    canUpgrade: true

                };
            }
        }
        window.isInsideBoard = this.isInsideBoard;
    }

    addNextTile() {
        let emptyTiles = [];
        for (let y = 0; y < 4; y++) {
            for (let x = 0; x < 4; x++) {
                if (this.tiles[y][x].value == 0) {
                    emptyTiles.push({
                        x: x,
                        y: y
                    });
                }
            }
        }
        let randomTile = Phaser.Utils.Array.GetRandom(emptyTiles);
        this.tiles[randomTile.y][randomTile.x].value = 2;
        this.animateNew(randomTile.x, randomTile.y);

    }

    makeTurn(dx, dy) {
        console.log("make turn", dx, dy)
        if (dx === 1) {
            console.log(this.tiles);
            Phaser.Utils.Array.RotateLeft(this.tiles, 2)
            console.log(this.tiles);
            this.turn(dx, dy)
            Phaser.Utils.Array.RotateRight(this.tiles, 2)
        } else {
            this.turn(dx, dy)
        }

    }

    turn(dx, dy) {
        for (let j = 0; j < 4; j++) {
            for (let i = 0; i < 4; i++) {
                let x = dx == 1 ? 3 - i : i;
                let y = dy == 1 ? 3 - j : j;
                if (this.tiles[y][x].value != 0) {
                    let stepsX = dx;
                    let stepsY = dy;
                    while (this.isInsideBoard(x + stepsX, y + stepsY) && this.tiles[y + stepsY][x + stepsX].value == 0) {
                        stepsX += dx;
                        stepsY += dy;
                    }
                    if (this.isInsideBoard(x + stepsX, y + stepsY) && this.tiles[y + stepsY][x + stepsX].value === this.tiles[y][x].value &&
                        this.tiles[y + stepsY][x + stepsX].canUpgrade && this.tiles[y][x].canUpgrade) {
                        this.tiles[y + stepsY][x + stepsX].value *= 2;
                        this.tiles[y + stepsY][x + stepsX].canUpgrade = false;
                        this.tiles[y][x].value = 0;
                        this.animateMove(x, y, stepsX, stepsY, true);
                    } else {
                        stepsX -= dx;
                        stepsY -= dy;
                        if (stepsX != 0 || stepsY != 0) {
                            this.tiles[y + stepsY][x + stepsX].value = this.tiles[y][x].value;
                            this.tiles[y][x].value = 0;
                            this.animateMove(x, y, stepsX, stepsY, false);
                        }
                    }
                }
            }
        }
        for (let y = 0; y < 4; y++) {
            for (let x = 0; x < 4; x++) {
                this.tiles[y][x].canUpgrade = true;
            }
        }
    }


    isInsideBoard(x, y) {
        return (x >= 0 && x < 4 && y >= 0 && y < 4)
    }

    animateNew(x, y) {
        if (typeof this.onNew === "function") {
            this.onNew(x, y);
        } else {
            console.error("onNew function is not defined")
        }
    }

    animateMove(xx, yy, dx, dy, merged) {
        if (typeof this.onMove === "function") {
            this.onMove(xx, yy, dx, dy, merged);
        } else {
            console.error("onMove function is not defined")
        }
    }
}