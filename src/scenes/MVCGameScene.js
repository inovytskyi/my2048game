import 'phaser';
import Model from "../objects/model";

export default class MVCGameScene extends Phaser.Scene {
    constructor() {
        super('MVCGame');

        this.tilesize = 200;
        this.scale = 1;
        this.startPosition = 10;
        this.tileSprites = [];
        this.tween_duration = 50;
        this.can_be_moved = true;
        this.animated_sprites = 0;


    }

    preload() {
        this.texture = this.load.spritesheet('tile', 'assets/tile.png', {
            frameWidth: this.tilesize / this.scale
        });
    }

    create() {
        this.game.resize();
        this.model = new Model()

        this.model.onNew = (x, y) => {
            this.tileSprites[y][x].setFrame(this.getFrame(this.model.tiles[y][x].value));
            this.tweens.add({
                targets: this.tileSprites[y][x],
                alpha: 1,

                onComplete: function (tween) {

                }
            });

        }
        this.model.onMove = (xx, yy, dx, dy, merged) => {
            let move_length = Math.abs(dx + dy);
            if (move_length != 0) {
                this.can_be_moved = false;
                this.animated_sprites++;
                let target_x = this.getPosition(xx + dx);
                let target_y = this.getPosition(yy + dy);

                this.tweens.add({
                    targets: [this.tileSprites[yy][xx]],
                    x: target_x,
                    y: target_y,
                    duration: move_length * this.tween_duration,
                    onComplete: function (tween) {
                        tween.parent.scene.animated_sprites--;
                        if (merged) {
                            tween.parent.scene.animated_sprites++
                            tween.parent.scene.tileSprites[yy][xx].depth = 2;
                            tween.parent.scene.tweens.add({
                                targets: [tween.parent.scene.tileSprites[yy][xx]],
                                scaleX: 1.1,
                                scaleY: 1.1,
                                duration: tween.parent.scene.tween_duration,
                                yoyo: true,
                                repeat: 1,
                                onComplete: function (tw) {
                                    tw.parent.scene.animated_sprites--;
                                    if (tw.parent.scene.animated_sprites == 0) {
                                        tw.parent.scene.can_be_moved = true;
                                        tw.parent.scene.sync();
                                    }
                                }


                            });
                        }

                        if (tween.parent.scene.animated_sprites == 0) {
                            tween.parent.scene.can_be_moved = true;
                            tween.parent.scene.sync();
                        }
                    }
                });

            }

            if (this.animated_sprites === 0) {
                this.can_be_moved = true;
                this.sync();
            }



        }

        for (let y = 0; y < 4; y++) {
            this.tileSprites[y] = [];
            for (let x = 0; x < 4; x++) {
                this.tileSprites[y][x] = this.add.tileSprite(this.getPosition(x), this.getPosition(y),
                    this.tilesize, this.tilesize, 'tile', 0);
                this.tileSprites[y][x].setOrigin(0);
                this.tileSprites[y][x].setTileScale(this.scale, this.scale);
                this.tileSprites[y][x].alpha = 0;
            }
        }

        this.input.keyboard.on("keydown", this.handleKey, this);
        this.input.on("pointerup", this.endSwipe, this);
        this.model.addNextTile();
        this.model.addNextTile();

    }

    update() {

    }

    handleKey(e) {
        if (this.can_be_moved) {
            if (e.code === "Space") {
                this.model.addNextTile()
            }
            if (e.code == "ArrowLeft") {
                this.model.turn(-1, 0);
            }
            if (e.code == "ArrowRight") {
                this.model.turn(1, 0);
            }
            if (e.code == "ArrowUp") {
                this.model.turn(0, -1);
            }
            if (e.code == "ArrowDown") {
                this.model.turn(0, 1);
            }
        }

    }

    endSwipe(e) {
        var swipeTime = e.upTime - e.downTime;
        var swipe = new Phaser.Geom.Point(e.upX - e.downX, e.upY - e.downY);
        var swipeMagnitude = Phaser.Geom.Point.GetMagnitude(swipe);
        var swipeNormal = new Phaser.Geom.Point(swipe.x / swipeMagnitude, swipe.y / swipeMagnitude);
        if (swipeMagnitude > 20 && swipeTime < 1000 && (Math.abs(swipeNormal.y) > 0.8 || Math.abs(swipeNormal.x) > 0.8)) {
            if (swipeNormal.x > 0.8) {
                this.model.turn(1, 0);
            }
            if (swipeNormal.x < -0.8) {
                this.model.turn(-1, 0);
            }
            if (swipeNormal.y > 0.8) {
                this.model.turn(0, 1);
            }
            if (swipeNormal.y < -0.8) {
                this.model.turn(0, -1);
            }
        }
    }

    sync() {
        for (let y = 0; y < 4; y++) {
            for (let x = 0; x < 4; x++) {
                if (this.model.tiles[y][x].value == 0) {
                    this.tileSprites[y][x].alpha = 0;
                } else {
                    this.tileSprites[y][x].alpha = 1;
                    this.tileSprites[y][x].depth = 1;
                    this.tileSprites[y][x].setFrame(this.getFrame(this.model.tiles[y][x].value));
                }
                this.tileSprites[y][x].setPosition(this.getPosition(x), this.getPosition(y));
            }
        }
        this.model.addNextTile();
    }

    getPosition(coordinate) {
        return coordinate * this.tilesize + this.startPosition;
    }

    getFrame(value) {
        return Math.round(Math.log2(value) - 1)
    }

}